package it.prismaprogetti.academy.mastermind.model;

import static it.prismaprogetti.academy.mastermind.model.EsitoPartita.PERSA;
import static it.prismaprogetti.academy.mastermind.model.EsitoPartita.VINTA;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import it.prismaprogetti.academy.mastermind.utility.RandomUtility;

public class Partita {

	private static int contatorePartite = 0;
	private String nome;
	private int numeroTentativi;
	private EsitoPartita esito;
	private LocalTime tempoDiGioco;

	private Partita(String nome, int numeroTentativi, EsitoPartita esito, LocalTime tempoDiGioco,
			boolean tornareAlMenu) {
		super();
		this.nome = nome;
		this.numeroTentativi = numeroTentativi;
		this.esito = esito;
		this.tempoDiGioco = tempoDiGioco;

		if (tornareAlMenu) {
			MasterMind.partite.put(this.getNome(), this);
			MasterMind.gioca();

		}
	}

	public static int getContatorePartite() {
		return contatorePartite;
	}

	public String getNome() {
		return nome;
	}

	public int getNumeroTentativi() {
		return numeroTentativi;
	}

	public EsitoPartita getEsito() {
		return esito;
	}

	public LocalTime getTempoDiGioco() {
		return tempoDiGioco;
	}

	public static Partita creaPartita(Scanner input) {

		int numeroTentativi = 0;
		EsitoPartita esitoPartita = null;
		boolean finePartita = false;
		LocalTime startTimeGame = null;
		LocalTime endTimeGame = null;

		while (!finePartita) {

			int numeroColori = 0;
			int numeroPosizioni = 0;
			String coloriScelti = null;

			numeroTentativi = chiediNumeroTentativi(input);
			numeroPosizioni = chiediNumeroPosizioni(input);
			numeroColori = chiediNumeroColori(input);

			/*
			 * scelgo casualmente i colori, senza ripetizione, che potranno essere
			 * utilizzati nel gioco
			 */
			Colore[] coloriDisponibiliPerLaCombinazione = generazioneColoriDisponibili(numeroColori);
			/*
			 * scelgo casualmente la combinazione colori con ripetizione che dovr� essere
			 * indovinata dal giocatore
			 */
			Colore[] combinazioneColoriDaTrovare = generazioneCombinazioneColori(numeroPosizioni,
					coloriDisponibiliPerLaCombinazione);

			System.out.println(
					"sono stati creati i seguenti colori casuali che puoi sceglire per formare una combinazione:");
			for (Colore colore : coloriDisponibiliPerLaCombinazione) {
				System.out.println(colore.toString());
			}
			System.out.println("\n------------------------------------------------");
			System.out.println("Numero tentativi: " + numeroTentativi);
			System.out.println("Numero colori: " + numeroColori);
			System.out.println("Numero posizioni: " + numeroPosizioni);
			System.out.println("------------------------------------------------\n");

			/*
			 * inizio tempo di gioco
			 */
			startTimeGame = LocalTime.now();

			for (int tentativoCorrente = 0; tentativoCorrente < numeroTentativi; tentativoCorrente++) {

				String risultato = null;

				do {

					try {

						System.out.println(
								"Tentativo " + (tentativoCorrente + 1) + "/" + numeroTentativi + ". Inserisci i "
										+ coloriDisponibiliPerLaCombinazione.length + " colori separati dalla virgola");

						coloriScelti = input.nextLine();
						risultato = scovaCombinazioneColori(coloriScelti, combinazioneColoriDaTrovare,
								coloriDisponibiliPerLaCombinazione);
					} catch (PatternSyntaxException e) {
					} catch (InputMismatchException e) {
					}

				} while (risultato == null);

				System.out.println("---------------------------------");
				System.out.println("Esito: " + risultato);
				System.out.println("---------------------------------");

				if (isWinner(risultato, combinazioneColoriDaTrovare)) {

					finePartita = true;
					esitoPartita = VINTA;
					break;
				}

				if (tentativoCorrente == numeroTentativi - 1) {

					System.out.println("Hai perso");
					finePartita = true;
					esitoPartita = PERSA;
				}

			}

			/*
			 * fine tempo partita
			 */
			endTimeGame = LocalTime.now();

		}

		/*
		 * tempi
		 */
		int hours = (int) startTimeGame.until(endTimeGame, ChronoUnit.HOURS);
		int minute = (int) startTimeGame.until(endTimeGame, ChronoUnit.MINUTES);
		int seconds = (int) startTimeGame.until(endTimeGame, ChronoUnit.SECONDS);
		LocalTime tempoDiGioco = LocalTime.of(hours, minute, seconds);
		System.out.println("il gioco � durato: " + tempoDiGioco);

		String nuovaPartita = null;
		boolean tornareAlMenu = false;
		do {
			System.out.println("Vuoi tornare al menu? digita si o no");
			nuovaPartita = input.next().trim();

			if (nuovaPartita.equalsIgnoreCase("si")) {
				tornareAlMenu = true;
				break;
			} else if (nuovaPartita.equalsIgnoreCase("no")) {
				System.out.println("exit game");
				break;
			} else {
				System.err.println("hai digitato un comando non valido");
			}

		} while (true);

		return new Partita("partita" + ++contatorePartite, numeroTentativi, esitoPartita, tempoDiGioco, tornareAlMenu);
	}

	private static boolean isWinner(String risultato, Colore[] combinazioneColoriDaTrovare) {

		/*
		 * se la lunghezza del risultato � minore da quella della combinazione da
		 * trovare sicuramente non ho vinto
		 */
		if (risultato.length() < combinazioneColoriDaTrovare.length) {

			System.out.println("Non hai indovinato, ritenta sarai pi� fortunato");
			return false;
		}

		for (int j = 0; j < combinazioneColoriDaTrovare.length; j++) {

			if (risultato.charAt(j) != '+') {

				System.out.println("Non hai indovinato, ritenta sarai pi� fortunato");
				return false;
			}

		}

		System.out.println("COMPLIMENTI! Hai vinto");
		return true;
	}

	private static int chiediNumeroColori(Scanner input) {

		int numeroColori = 0;

		do {

			try {
				System.out.println(
						"inserisci il numero di colori che il programma sceglier� casualmente tra 2 e 6 con cui potrai giocare ");
				numeroColori = Integer.valueOf(input.nextLine());
				if (!isNumberInRange(2, 6, numeroColori)) {
					numeroColori = 0;
					continue;
				}
			} catch (NumberFormatException e) {
				System.err.println("errore number format exception");// TODO
			}

		} while (numeroColori == 0);

		return numeroColori;
	}

	private static int chiediNumeroTentativi(Scanner input) {

		int numeroTentativi = 0;

		do {

			try {
				System.out.println("inserisci un numero tentativi da 1 a 12");
				numeroTentativi = Integer.valueOf(input.nextLine());
				if (!isNumberInRange(1, 12, numeroTentativi)) {
					numeroTentativi = 0;
					continue;
				}
			} catch (NumberFormatException e) {
			}

		} while (numeroTentativi == 0);

		return numeroTentativi;
	}

	private static int chiediNumeroPosizioni(Scanner input) {

		int numeroPosizioni = 0;

		do {

			try {
				System.out.println("inserisci numero di posizioni da 3 a 6");
				numeroPosizioni = Integer.valueOf(input.nextLine());
				if (!isNumberInRange(3, 6, numeroPosizioni)) {
					numeroPosizioni = 0;
					continue;
				}
			} catch (NumberFormatException e) {
			}

		} while (numeroPosizioni == 0);

		return numeroPosizioni;

	}

	/**
	 * metodo che genera i colori che potranno essere utilizzati nel gioco
	 * 
	 * @param numeroMaxColori
	 * @return
	 */
	private static Colore[] generazioneColoriDisponibili(int numeroMaxColori) {

		Colore[] coloriGenerati = new Colore[numeroMaxColori];

		for (int i = 0; i < numeroMaxColori; i++) {

			String[] coloriDisponibili = Colore.COLORI_DISPONIBILI.split(",");

			int indiceArrayColoriDisponibili = RandomUtility.randomNumber(0, coloriDisponibili.length - 1);

			String coloreDisponibile = coloriDisponibili[indiceArrayColoriDisponibili];

			for (Colore colore : coloriGenerati) {

				if (colore == null) {

					coloriGenerati[i] = new Colore(coloreDisponibile, i + 1);
					break;
				}

				if (colore != null && coloreDisponibile.equals(colore.getNome())) {
					--i;
					break;
				}
			}

		}

		return coloriGenerati;

	}

	/**
	 * metodo che genera la combinazione di colori da indovinare
	 * 
	 * @param numeroPosizioni
	 * @param numeroColori
	 * @return
	 */
	private static Colore[] generazioneCombinazioneColori(int numeroPosizioni,
			Colore[] coloriDaUtilizzarePerLaCombinazione) {

		Colore[] combinazioneColori = new Colore[numeroPosizioni];

		for (int i = 0; i < numeroPosizioni; i++) {

			int indiceCasuale = RandomUtility.randomNumber(0, coloriDaUtilizzarePerLaCombinazione.length - 1);

			combinazioneColori[i] = coloriDaUtilizzarePerLaCombinazione[indiceCasuale];

		}

		return combinazioneColori;

	}

	private static String scovaCombinazioneColori(String coloriScelti, Colore[] coloriDaTrovare,
			Colore[] coloriDisponibili) throws PatternSyntaxException {

		String risultato = "";

		/*
		 * verifico che la formattazione stringa dei colori scelti rispetti il pattern
		 * definito
		 */
		String[] numeriColoriSceltiByString = checkIfRightPattern(coloriScelti, coloriDaTrovare.length);

		/*
		 * verifico che i colori scelti siano presenti nei colori disponibili
		 */
		if (!verificaColoriSceltiSonoInColoriDaTrovare(numeriColoriSceltiByString, coloriDisponibili)) {

			throw new InputMismatchException("devi scegliere i colori tra quelli indicati");
		}

		List<Integer> indiciArrayColoriDaTrovare = new ArrayList<>();

		/*
		 * ciclo dei +
		 */
		for (int i = 0; i < coloriDaTrovare.length; i++) {

			int numeroColoreDaTrovare = coloriDaTrovare[i].getNumero();

			if (numeroColoreDaTrovare == Integer.parseInt(numeriColoriSceltiByString[i])) {

				risultato += "+";
			} else {

				indiciArrayColoriDaTrovare.add(i);
			}

		}

		/*
		 * cicli dei -
		 */
		int qtaIndiciMancanti = indiciArrayColoriDaTrovare.size();

		/*
		 * impostazione qta colori da trovare: per ogni colore scelto che trovo aumento
		 * la sua quantit� di uno
		 */
		for (Colore colore : coloriDisponibili) {

			colore.setQta(0);

		}

		for (Colore colore : coloriDisponibili) {

			for (String s : numeriColoriSceltiByString) {

				int n = Integer.parseInt(s);

				if (colore.getNumero() == n) {

					int qtaColore = colore.getQta();
					colore.setQta(qtaColore + 1);
				}
			}

		}

		for (int k = 0; k < qtaIndiciMancanti; k++) {

			int numeroColoreDaTrovare = coloriDaTrovare[indiciArrayColoriDaTrovare.get(k)].getNumero();
			int qtaColoreDaTrovare = coloriDaTrovare[indiciArrayColoriDaTrovare.get(k)].getQta();

			for (int u = indiciArrayColoriDaTrovare.get(0); u < qtaIndiciMancanti; u++) {

				if (u != k && (qtaColoreDaTrovare != 0) && (numeroColoreDaTrovare == Integer
						.parseInt(numeriColoriSceltiByString[indiciArrayColoriDaTrovare.get(u)]))

				) {

					risultato += "-";
					coloriDaTrovare[indiciArrayColoriDaTrovare.get(k)].setQta(--qtaColoreDaTrovare);

					break; // interrompo for degli "u"

				}

			}

		}

		return risultato;

	}

	private static String[] checkIfRightPattern(String coloriScelti, int qtaColori) {

		String[] numeriColoriSceltiArrayByString = coloriScelti.replaceAll("//s+", coloriScelti).split(",");

		int numeroPosizioniDaTrovare = numeriColoriSceltiArrayByString.length;
		String regex = "";
		for (int i = 0; i < numeroPosizioniDaTrovare; i++) {

			if (i == numeroPosizioniDaTrovare - 1) {
				regex += "[\\d]";
				break;
			}
			regex += "[\\d],";
		}

		if (!Pattern.matches(regex, coloriScelti) || numeriColoriSceltiArrayByString.length != qtaColori) {

			throw new PatternSyntaxException("pattern non rispettato", regex, -1);
		}

		return numeriColoriSceltiArrayByString;

	}

	private static boolean verificaColoriSceltiSonoInColoriDaTrovare(String[] coloriSceltiByString,
			Colore[] coloriDisponibili) {

		int qtaColoriDaAbbinare = coloriDisponibili.length;
		
		for (int i = 0; i < coloriSceltiByString.length; i++) {

			int numeroColoreScelto = Integer.parseInt(coloriSceltiByString[i]);

			for (Colore colore : coloriDisponibili) {

				if (numeroColoreScelto == colore.getNumero()) {
					qtaColoriDaAbbinare--;
					break;
				}

			}
			
			if ( ( qtaColoriDaAbbinare + (i+1) ) != coloriDisponibili.length) {
				return false;
			}
			
		}

		return true;

	}

	public static boolean isNumberInRange(int min, int max, int num) {

		if (num < min || num > max) {
			System.err.println("inserisci numero da " + min + " a " + max);
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return "Partita [nome=" + nome + ", numero tentativi=" + numeroTentativi + ", esito partita=" + esito + "]";
	}

}
