package it.prismaprogetti.academy.mastermind.model;

public class Colore {

	static final String COLORI_DISPONIBILI = "ROSSO, BLU, GIALLO, VERDE, ARANCIO, VIOLA, BIANCO, NERO";
	private String nome;
	private int numero;
	private int qta;

	public Colore(String nome, int numero) {
		super();
		this.nome = nome;
		this.numero = numero;
	}

	public static String getColoriDisponibili() {
		return COLORI_DISPONIBILI;
	}

	public String getNome() {
		return nome;
	}

	public int getNumero() {
		return numero;
	}

	void setNumero(int numero) {
		this.numero = numero;
	}

	public int getQta() {
		return qta;
	}

	void setQta(int qta) {
		this.qta = qta;
	}

	@Override
	public String toString() {
		return this.getNumero() + ") " + this.getNome();
	}

	@Override
	public int hashCode() {
		return this.getNome().hashCode() + this.numero;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (obj instanceof Colore) {
			Colore colore = (Colore) obj;
			return colore.getNome().equals(this.getNome()) && colore.getNumero() == this.getNumero();
		}

		return false;
	}

}
