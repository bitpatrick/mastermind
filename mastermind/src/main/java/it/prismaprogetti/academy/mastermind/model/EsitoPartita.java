package it.prismaprogetti.academy.mastermind.model;

public enum EsitoPartita {

	VINTA, PERSA;
}
