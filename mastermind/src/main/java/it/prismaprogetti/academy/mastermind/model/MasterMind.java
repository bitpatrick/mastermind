package it.prismaprogetti.academy.mastermind.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class MasterMind {

	static Map<String, Partita> partite = new HashMap<>();

	public static void gioca() {

		boolean isWorking = true;
		while (isWorking) {

			Scanner input = new Scanner(System.in);
			System.out.println("MASTER MIND");
			System.out.println("Digita H per Help");
			System.out.println("Digita A per avvio partita");
			System.out.println("Digita Q per uscire");
			String parametro = input.nextLine().trim().toUpperCase();

			switch (parametro) {

			case "H":

				System.out.println("Hai cliccato su H");
				System.out.println(regoleDelGioco());
				break;

			case "A":

				System.out.println("Hai cliccato su avvia gioco");
				isWorking = false;
				/*
				 * sto creando il concetto di PArtita
				 */

				Partita.creaPartita(input);

				break;

			case "Q":

				System.out.println("Hai cliccato su esci dal programma");
				isWorking = false;
				input.close();
				break;

			case "P":

				if (partite.isEmpty()) {
					System.out.println("non hai effettuato alcuna partita");
				} else {

					Set<String> nomiPartite = partite.keySet();
					for (String nomePartita : nomiPartite) {
						System.out.println(partite.get(nomePartita));
					}

				}

				break;

			default:
				System.out.println("non hai digitato un parametro valido, riprova per favore");
				break;

			}

		}

	}

	private static String regoleDelGioco() {

		return "Mastermind � un gioco ...";
	}

}
